CPU: Rockchip RK3066 40nm Dual-core Cortex-A9, up to 1.6Ghz; Quad-core Mali 400 GPU,up to 400Mhz
Screen: HannStar Display model HSD101PWW1_A00
Touch:  Goodix GT828
Wifi:   MediaTek RT5370
Audio:  RTL5631
Sensor: Lis3dh MMA7660 (daemon MMA8452)
Keypad: Power=116, Vol+=115, Vol-=114
Battery:    DPC36119150C 3.7v 6800 MAH
Camera:     gc0308 front, hi253 rear
HDMI:   RK30
Clock:  TPS65910

